# Quo-data

Datasets used in "Quartet Coestimation of gene trees and species tree"

- Maryam Rabiee, Siavash Mirarab, QuCo: quartet-based co-estimation of species trees and gene trees, Bioinformatics, Volume 38, Issue Supplement_1, July 2022, Pages i413–i421, https://doi.org/10.1093/bioinformatics/btac265


## Datasets

- Quartet-simulation: Data related to quartet simulations in the paper including MrBayes outputs and Quco outputs
- Anomaly-simulation: Data related to anomaly simulations in the paper including Quco and Bucky outputs

## Google Drive link for large data

- Rest of datasets is available on [Dryad](https://datadryad.org/stash/dataset/doi:10.6076/D1CP4R)




